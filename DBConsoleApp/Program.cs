﻿ using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//TODO: Tee ohjelma valmiiksi:
//      Lisää SQL-Connection string kysely
//      Lisää listaus Tauluista ja Anna sen jälkeen kehote SQL kyselylle
//      Lisää virheenkäsittely toiminnot

/*Testailumielessä Kommentointia että näkee toimiiko gitti molemmissa päissä. Serverin CONNSTR täytyy laittaa kuntoon molemmilla.*/

/*Testailua, Uusi Branch luotu "LappariDevaus"*/

/*Luotu Desktop Branch kotikonetta varten*/
namespace DbConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int j = 0, i = 0, k = 0; //apumuuttujia SQL-Taulun tietojen lukemiseen.
            string sarakeNimi = "";  //Myos apumuuttuja
            string sarakeArvo = "";  //Kuin myos tama.
            string connStr = "Server=localhost\\SQLEXPRESS;Database=Northwind;Trusted_Connection=True;"; //DESKTOP-MHJTLHT lappari
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();

            string sql = "123";
            while (sql.ToUpper() != "X")
            {
                Console.Write("SQL> ");
                sql = Console.ReadLine();
                if (sql == "") continue;
                

                

                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                //PadRight toiminto varaa Merkistölle tietyn määrän tilaa. Substring komennolla varmistetaan että ei tule ylivuotoa.
                /*Kova koodattu Sql haku....
                Console.WriteLine("ID ".PadRight(5).Substring(0,5)+"Company Name ".PadRight(20).Substring(0,20)+"Phone ".PadRight(15).Substring(0,15));
                */

                DataTable schemaTable = reader.GetSchemaTable(); //Luodaan schemaTable niminen olio joka lukee SQL-tietokannan tauluja.
                foreach (DataRow row in schemaTable.Rows) //Kaydaan lapi jokainen rivi joka loytyy.
                {
                    foreach (DataColumn column in schemaTable.Columns) //Kaydaan lapi jokainen sarake.
                    {
                        if (column.ColumnName == "ColumnName")
                        {
                            j++; //Aina kun uusi sarake haetaan, siirretaan jiita yhdella eteeenpain.
                            sarakeNimi = row[column].ToString();
                            sarakeNimi = (sarakeNimi.PadRight(15).Substring(0, 15) + "||");
                            Console.Write(sarakeNimi);
                        }
                    }
                }
                Console.WriteLine();
                foreach (DataRow row in schemaTable.Rows) //Kaytetaan samaa koodia kuin ylla ja lisataan Sarakkeen nimien alle viiva, selkeyttamaan.
                {
                    foreach (DataColumn column in schemaTable.Columns)
                    {
                        if (column.ColumnName == "ColumnName")
                        {

                            sarakeNimi = row[column].ToString();
                            sarakeNimi = (sarakeNimi.PadRight(15).Substring(0, 15) + "||");

                            Console.Write("---------------||");
                        }
                    }
                }
                Console.WriteLine();



                while (reader.Read())
                {
                    i++; //Rivien laskeminen.
                    for (k = 0; k < j; k++)
                    {
                        sarakeArvo = reader.GetValue(k).ToString();
                        sarakeArvo = (sarakeArvo.PadRight(15).Substring(0, 15) + "||");
                        if (k < j - 1)
                        {
                            Console.Write(sarakeArvo);
                        }
                        else
                            Console.WriteLine(sarakeArvo);
                    }

                    //    int id = reader.GetInt32(0);
                    //    string companyName = reader.GetString(1);
                    //    string phone = reader.GetString(2);
                    //    Console.WriteLine(id.ToString().PadRight(5).Substring(0,5) + companyName.PadRight(20).Substring(0,20)+ phone.PadRight(15).Substring(0,15));
                }
            //Console.ReadLine(); 
            //Resurssien vapautus
            reader.Close();
            cmd.Dispose();
            schemaTable.Dispose();
                j = 0;
                k = 0;
                i = 0;
            }
            conn.Dispose();
        }
    }
}
